package kr.re.keti.demo.bluetooth;

import android.annotation.TargetApi;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import java.util.List;

import kr.re.keti.demo.fragment.MainFirstFragment;

/**
 * Created by eajang on 15. 9. 7..
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class BluetoothConnectivity_over21 extends BluetoothConnectivity {

    private BluetoothLeScanner mLeScanner = null;

    public BluetoothConnectivity_over21(Context mContext, MainFirstFragment.BtDeviceListAdapter mAdapter) {
        super(mContext, mAdapter);
    }

    @Override
    public void initializeBtService() {
        super.initializeBtService();
        mLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
    }

    @Override
    public void scanBtDevice(final boolean enable) {
        super.scanBtDevice(enable);
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mLeScanner.stopScan(scanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mLeScanner.startScan(scanCallback);
        } else {
            mScanning = false;
            mLeScanner.stopScan(scanCallback);
        }
        // TODO: menu item update
    }

    // Device scan callback for v21
    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            mAdapter.addDevice(result.getDevice());
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.e("BleConn | onScanFailed", ""+errorCode);
        }
    };

}
