package kr.re.keti.demo.fragment;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import kr.re.keti.demo.R;
import kr.re.keti.demo.activity.UserProfileActivity;
import kr.re.keti.demo.bluetooth.BluetoothConnectivity;
import kr.re.keti.demo.bluetooth.BluetoothConnectivity_under21;
import kr.re.keti.demo.bluetooth.BluetoothConnectivity_over21;
import kr.re.keti.demo.bluetooth.DeviceControlActivity;
import kr.re.keti.demo.db.UserProfile;

/**
 * Created by eajang on 15. 9. 9..
 */
public class MainFirstFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private ImageButton mBtnAddDevice;
    private CoordinatorLayout mCoordinator;
    private FloatingActionButton mBtnScanBtDevice;

    private RecyclerView mRecyclerView;
    private BtDeviceListAdapter mBtDeviceListAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private BluetoothConnectivity bleConn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_first, container, false);

        mContext = this.getActivity();

        // Set UI components
        mBtnAddDevice = (ImageButton) view.findViewById(R.id.main_first_imgbtn_add_device);
        mBtnAddDevice.setOnClickListener(this);

        mCoordinator = (CoordinatorLayout)view.findViewById(R.id.device_scan_layout);
        mCoordinator.setVisibility(View.GONE);
        //mTextViewScanDevice = (TextView) view.findViewById(R.id.device_scan_textview);
        //mTextViewScanDevice.setText(getResources().getText(R.string.title_devices));
        mBtnScanBtDevice = (FloatingActionButton) view.findViewById(R.id.device_scan_button);
        mBtnScanBtDevice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (mBtDeviceListAdapter.getItemCount() > 0) {
                        mBtDeviceListAdapter.clear();
                        mBtDeviceListAdapter.notifyDataSetChanged();
                    }
                    Animation rotate = AnimationUtils.loadAnimation(mContext, R.anim.rotate);
                    mBtnScanBtDevice.startAnimation(rotate);
                    bleConn.scanBtDevice(true);
                    return true;
                }
                return true;
            }
        });

        // RecyclerView settings
        mRecyclerView = (RecyclerView) view.findViewById(R.id.device_card_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mBtDeviceListAdapter = new BtDeviceListAdapter();
        mRecyclerView.setAdapter(mBtDeviceListAdapter);

        // Bluetooth connectivity object & initializing
        if (Build.VERSION.SDK_INT < 21) {
            bleConn = new BluetoothConnectivity_under21(mContext, mBtDeviceListAdapter);
        } else {
            bleConn = new BluetoothConnectivity_over21(mContext, mBtDeviceListAdapter);
        }
        bleConn.checkBleSupported();
        bleConn.initializeBtService();
        //if (bInitSuccess)
        //    bleConn.scanBtDevice(true);

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id) {
            case R.id.main_first_imgbtn_add_device:
                mBtnAddDevice.setVisibility(View.INVISIBLE);
                mCoordinator.setVisibility(View.VISIBLE);
                Animation slide_up = AnimationUtils.loadAnimation(mContext, R.anim.slide_up);
                mCoordinator.startAnimation(slide_up);
                break;

        }
    }

    // Adapter for holding devices found through scanning.
    public class BtDeviceListAdapter extends RecyclerView.Adapter<BtDeviceListAdapter.ViewHolder> {

        private ArrayList<BluetoothDevice> mLeDevices;

        public BtDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
        }

        public void addDevice(BluetoothDevice device) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).
                    inflate(R.layout.layout_device_scan_card_list, viewGroup, false);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = mRecyclerView.getChildLayoutPosition(v);
                    Intent intent = new Intent(mContext, UserProfileActivity.class);
                    intent.putExtra(UserProfileActivity.EXTRAS_KEY_USER_EDIT_MODE, UserProfileActivity.EXTRAS_VALUE_USER_NEW);
                    intent.putExtra("device_address", mLeDevices.get(position).getAddress());
                    getActivity().startActivity(intent);
                }
            });
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            final String deviceAddress = device.getAddress();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.mDeviceName.setText(deviceName);
            else
                viewHolder.mDeviceName.setText(R.string.unknown_device);
            viewHolder.mDeviceAddress.setText(deviceAddress);
            viewHolder.mBtnDeviceInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, DeviceControlActivity.class);
                    intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, deviceName);
                    intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, deviceAddress);
                    getActivity().startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mLeDevices.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView mDeviceName;
            public TextView mDeviceAddress;
            public ImageButton mBtnDeviceInfo;

            public ViewHolder(View itemView) {
                super(itemView);
                mDeviceName = (TextView) itemView.findViewById(R.id.device_card_tv_name);
                mDeviceAddress = (TextView) itemView.findViewById(R.id.device_card_tv_address);
                mBtnDeviceInfo = (ImageButton) itemView.findViewById(R.id.device_card_btn_info);
            }
        }

    }
}
