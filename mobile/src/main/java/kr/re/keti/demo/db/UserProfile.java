package kr.re.keti.demo.db;

import java.io.Serializable;


/**
 * Created by eajang on 15. 10. 6..
 */
public class UserProfile implements Serializable{

    private String mUserName;
    private String mDeviceAddress;


    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getDeviceAddress() {
        return mDeviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.mDeviceAddress = deviceAddress;
    }
}
