package kr.re.keti.demo.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

import kr.re.keti.demo.R;
import kr.re.keti.demo.bluetooth.BluetoothDeviceInfo;
import kr.re.keti.demo.db.UserProfile;

public class UserProfileActivity extends AppCompatActivity {

    public static final String EXTRAS_KEY_USER_EDIT_MODE = "USER_EDIT_MODE";
    public static final String EXTRAS_VALUE_USER_NEW = "USER_NEW";
    public static final String EXTRAS_VALUE_USER_EDIT = "USER_EDIT";

    private boolean m_bIsNewUser;

    private TextView m_tvUserName;
    private EditText m_etUserName;
    private ImageButton m_btnEditUserName;
    private ImageButton m_btnCheckUserName;
    private Button m_btnSave;
    private TextView m_tvDeviceAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userprofile);

        // View Components
        m_tvUserName = (TextView)findViewById(R.id.user_profile_user_name);
        m_etUserName = (EditText)findViewById(R.id.user_profile_user_name_edit);
        m_tvDeviceAddress = (TextView)findViewById(R.id.user_profile_device_address);
        m_btnEditUserName = (ImageButton)findViewById(R.id.user_profile_user_name_btn_edit);
        m_btnEditUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_etUserName.setText(m_tvUserName.getText().toString());
                m_etUserName.setVisibility(View.VISIBLE);
                m_btnEditUserName.setVisibility(View.GONE);
                m_btnCheckUserName.setVisibility(View.VISIBLE);
                m_tvUserName.setVisibility(View.GONE);
            }
        });
        m_btnCheckUserName = (ImageButton)findViewById(R.id.user_profile_user_name_btn_check);
        m_btnCheckUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_tvUserName.setText(m_etUserName.getText().toString());
                m_etUserName.setVisibility(View.GONE);
                m_btnCheckUserName.setVisibility(View.GONE);
                m_btnEditUserName.setVisibility(View.VISIBLE);
                m_tvUserName.setVisibility(View.VISIBLE);
            }
        });
        m_btnSave = (Button)findViewById(R.id.user_profile_btn_save);
        m_btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = m_tvUserName.getText().toString();
                String deviceAddress = m_tvDeviceAddress.getText().toString();
                UserProfile mUserProfile = new UserProfile();
                mUserProfile.setUserName(userName);
                mUserProfile.setDeviceAddress(deviceAddress);
                Intent intent = new Intent(UserProfileActivity.this, MainActivity.class);
                intent.putExtra("USER", mUserProfile);
                startActivity(intent);
            }
        });

        // Intent Extras
        Intent intent = getIntent();
        String editMode = intent.getExtras().getString(EXTRAS_KEY_USER_EDIT_MODE);
        if (editMode.equals(EXTRAS_VALUE_USER_NEW)) {
            m_bIsNewUser = true;
        } else if (editMode.equals(EXTRAS_VALUE_USER_EDIT)) {
            m_bIsNewUser = false;
        }

        String sAddr = intent.getExtras().getString("device_address");
        if (sAddr.equals("")||sAddr == null) finish();
        else {
            m_tvDeviceAddress.setText(sAddr);
        }

        // Settings Default Value
        if (m_bIsNewUser) {
            // New User
            m_etUserName.setVisibility(View.VISIBLE);
            m_tvUserName.setVisibility(View.GONE);
            m_btnEditUserName.setVisibility(View.GONE);
            m_btnCheckUserName.setVisibility(View.VISIBLE);
        } else {
            // Edit User Info
            m_etUserName.setVisibility(View.GONE);
            m_tvUserName.setVisibility(View.VISIBLE);
            m_btnEditUserName.setVisibility(View.VISIBLE);
            m_btnCheckUserName.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
