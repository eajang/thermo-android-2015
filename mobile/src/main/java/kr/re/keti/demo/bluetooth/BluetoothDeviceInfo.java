package kr.re.keti.demo.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.os.ParcelUuid;


/**
 * Created by eajang on 15. 9. 7..
 */
public class BluetoothDeviceInfo {


    private String mDeviceName;
    private String mDeviceAddress;
    private Color mIdColor;
    private ParcelUuid mUuids;
    private BluetoothDevice mBtDevice;
    private int mRssi;

    public void setDeviceName(String mDeviceName) {
        this.mDeviceName = mDeviceName;
    }

    public void setUuids(ParcelUuid mUuids) {
        this.mUuids = mUuids;
    }

    public void setBtDevice(BluetoothDevice mBtDevice) {
        this.mBtDevice = mBtDevice;
    }

    public void setIdColor(Color mIdColor) {
        this.mIdColor = mIdColor;
    }

    public void setRssi(int mRssi) {
        this.mRssi = mRssi;
    }

    public String getDeviceName() {
        return mDeviceName;
    }

    public ParcelUuid getUuids() {
        return mUuids;
    }

    public BluetoothDevice getBtDevice() {
        return mBtDevice;
    }

    public int getRssi() {
        return mRssi;
    }

    public Color getIdColor() {
        return mIdColor;
    }

    public String getDeviceAddress() {
        return mDeviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.mDeviceAddress = deviceAddress;
    }
}
