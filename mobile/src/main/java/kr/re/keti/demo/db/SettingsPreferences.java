package kr.re.keti.demo.db;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by eajang on 15. 9. 9..
 */
public class SettingsPreferences {

    private final String PREF_NAME = "kr.re.keti.demo.db.pref";

    public final static String PREF_HAVE_DATA = "PREF_HAVE_DATA";
    public final static String PREF_DEFAULT_DEVICE = "PREF_DEFAULT_DEVICE";
    public final static String PREF_DEFAULT_DEVICE_UUID = "DEVICE_UUID";

    private static Context mContext;

    /**
     * Constructor
     * @param mContext
     */
    public SettingsPreferences(Context mContext) {
        this.mContext = mContext;
    }


    /**
     * Set String value in the Preferences editor
     * @param key The name of the preference to modify
     * @param value The new value for the preference
     */
    public void putValue(String key, String value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Set boolean value in the Preferences editor
     * @param key The name of the preference to modify
     * @param value The new value for the preference
     */
    public void putValue(String key, boolean value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Set int value in the Preferences editor
     * @param key The name of the preference to modify
     * @param value The new value for the preference
     */
    public void putValue(String key, int value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * Return String value
     * @param key The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist
     * @return String
     */
    public String getValue(String key, String defValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        try{
            return pref.getString(key, defValue);
        } catch(Exception e) {
            return defValue;
        }
    }

    /**
     * Return boolean value
     * @param key The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist
     * @return boolean
     */
    public boolean getValue(String key, boolean defValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        try{
            return pref.getBoolean(key, defValue);
        } catch(Exception e) {
            return defValue;
        }
    }

    /**
     * Return int value
     * @param key The name of the preference to retrieve.
     * @param defValue Value to return if this preference does not exist
     * @return int
     */
    public int getValue(String key, int defValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
        try{
            return pref.getInt(key, defValue);
        } catch(Exception e) {
            return defValue;
        }
    }
}
