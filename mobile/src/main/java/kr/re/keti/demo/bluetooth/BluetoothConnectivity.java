package kr.re.keti.demo.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Set;

import kr.re.keti.demo.fragment.MainFirstFragment;

/**
 * Created by eajang on 15. 9. 16..
 */
public class BluetoothConnectivity {

    private int mDeviceSDKVersion;

    protected boolean isSupportedBLE = false;
    protected Context mContext = null;
    protected MainFirstFragment.BtDeviceListAdapter mAdapter;

    protected Handler mHandler = null;
    public static boolean mScanning = false;
    // Stops scanning after 8 seconds.
    protected static final long SCAN_PERIOD = 8000;

    protected BluetoothManager mBluetoothManager = null;
    protected BluetoothAdapter mBluetoothAdapter = null;
    public static Set<BluetoothDevice> mBtDevices = null;

    public BluetoothConnectivity(Context mContext, MainFirstFragment.BtDeviceListAdapter mAdapter) {
        this.mContext = mContext;
        this.mBtDevices = new HashSet<BluetoothDevice>();
        this.mAdapter = mAdapter;
    }

    public void initializeBtService() {

        mHandler = new Handler();

        // Initialize bluetooth adapter
        mBluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            mContext.startActivity(enableBtIntent);
        }
        // Checking paired devices exist
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            // TODO: settings for pairedDevices
        }
    }

    public void checkBleSupported() {

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            // exception
        }

        // Checking ble is supported
        if(mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            isSupportedBLE = true;
            Toast.makeText(mContext, "This device is supporting ble service.", Toast.LENGTH_SHORT).show();
        }else {
            isSupportedBLE = false;
            Toast.makeText(mContext, "This device is not supporting ble service.", Toast.LENGTH_SHORT).show();
        }
    }

    public void setDeviceSDKVersion(int deviceSDKVersion) {
        this.mDeviceSDKVersion = deviceSDKVersion;
    }

    public int getDeviceSDKVersion() {
        return mDeviceSDKVersion;
    }

    public void scanBtDevice(boolean enable){};
}
