package kr.re.keti.demo.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.util.Log;

import kr.re.keti.demo.fragment.MainFirstFragment;

/**
 * Created by eajang on 15. 9. 7..
 */

public class BluetoothConnectivity_under21 extends BluetoothConnectivity {


    public BluetoothConnectivity_under21(Context mContext, MainFirstFragment.BtDeviceListAdapter mAdapter) {
        super(mContext, mAdapter);
    }

    @Override
    public void initializeBtService() {
        super.initializeBtService();
    }

    @Override
    public void scanBtDevice(final boolean enable) {
        super.scanBtDevice(enable);
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        // TODO: menu item update
    }

    // Device scan callback
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    new Runnable() {
                        @Override
                        public void run() {
                            Log.d("debug-adapter update", device.getAddress());
                            mAdapter.addDevice(device);
                            mAdapter.notifyDataSetChanged();
                        }
                    };
                }
            };

}
