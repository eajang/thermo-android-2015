package kr.re.keti.demo.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import kr.re.keti.demo.R;
import kr.re.keti.demo.db.SettingsPreferences;
import kr.re.keti.demo.db.UserProfile;
import kr.re.keti.demo.fragment.GraphFragment;
import kr.re.keti.demo.fragment.MainFirstFragment;
import kr.re.keti.demo.fragment.MainFragment;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // Fragment
    private FrameLayout mFragmentContainer;
    private MainFirstFragment mMainFirstFragment;
    private MainFragment mMainFragment;
    private GraphFragment mGraphFragment;

    // DrawerLayout
    private DrawerLayout mDrawerlayout;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boolean hasData = hasDefaultData();

        Intent intent = getIntent();
        if (intent.hasExtra("USER")) {
            UserProfile user = (UserProfile) intent.getSerializableExtra("USER");
            hasData = true;
        }
        // Fragment Settings
        mFragmentContainer = (FrameLayout) findViewById(R.id.main_container);
        mMainFirstFragment = new MainFirstFragment();
        mMainFragment = new MainFragment();
        mGraphFragment = new GraphFragment();

        // Load SharedPreferences
        if (!hasData) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_container, mMainFirstFragment);
            transaction.commit();
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_container, mMainFragment);
            transaction.commit();
        }

        // NavigationView Settings
        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);

        // Toolbar Settings
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        // NavigationDrawer Settings
        mDrawerlayout = (DrawerLayout) findViewById(R.id.main_drawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerlayout,
                toolbar,
                R.string.drawer_menu_open,
                R.string.drawer_menu_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerlayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        if(menuItem.isChecked()) menuItem.setChecked(false);
        else menuItem.setChecked(true);

        mDrawerlayout.closeDrawers();

        FragmentTransaction transaction = null;

        switch(menuItem.getItemId()) {
            case R.id.drawer_menu_main:
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_container, mMainFragment);
                transaction.commit();
                return true;
            case R.id.drawer_menu_01:
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_container, mGraphFragment);
                transaction.commit();
                Toast.makeText(getApplicationContext(), "Graph!!", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.drawer_menu_02:
                Toast.makeText(getApplicationContext(), "Settings!!", Toast.LENGTH_SHORT).show();
                return true;
        }
        return false;
    }

    private boolean hasDefaultData() {
        SettingsPreferences preferences = new SettingsPreferences(this);
        return preferences.getValue(SettingsPreferences.PREF_HAVE_DATA, false);
    }
}
